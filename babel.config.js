
process.env.VUE_CLI_BABEL_TRANSPILE_MODULES = true
process.env.VUE_CLI_BABEL_TARGET_NODE = 'node'

module.exports = {
  env: {
    test: {
      plugins: [
        // '@babel/plugin-transform-modules-commonjs',
        // '@babel/plugin-transform-runtime',
        // 'transform-es2015-modules-commonjs',
        // '@babel/plugin-syntax-dynamic-import',
        // '@babel/plugin-transform-async-to-generator',
        // '@babel/plugin-proposal-object-rest-spread',
        // 'transform-es2015-destructuring'
      ]
    }
  },
  presets: [
    '@vue/app'
  ]
}
