
export const getExpeditions = jest.fn(() => Promise.resolve([]))
export const getSteps = jest.fn(() => Promise.resolve([]))
export const getTraces = jest.fn(() => Promise.resolve([]))
export const getUsers = jest.fn(() => Promise.resolve([]))
