/**
 * The api/nomadMaps module is responsible for talking with the nomad maps REST API.
 * Each function in this module do the following things :
 *  - make HTTP requests to talk with the API
 *  - return payload returned by these requests in a Promise
 */

import { NOMAD_MAPS_SERVER } from '@/constants'
import { makeRequest } from '@/util/networking'

export async function getTraces (expeditionId) {
  return makeRequest(NOMAD_MAPS_SERVER, 'traces', { expeditions: expeditionId })
}

export async function getSteps (expeditionId) {
  return makeRequest(NOMAD_MAPS_SERVER, 'steps', { expeditions: expeditionId })
}

export async function getExpeditions (ids) {
  const params = ids ? { ids: ids.join(',') } : undefined
  return makeRequest(NOMAD_MAPS_SERVER, 'expeditions', params)
}

export async function getUsers (ids) {
  const params = ids ? { ids: ids.join(',') } : undefined
  return makeRequest(NOMAD_MAPS_SERVER, 'users', params)
}
