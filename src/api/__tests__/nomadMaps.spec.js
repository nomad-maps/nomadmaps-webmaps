
import flushPromises from 'flush-promises'
import { getTraces, getSteps, getExpeditions, getUsers } from '../nomadMaps'
import { makeRequest } from '@/util/networking'
import { NOMAD_MAPS_SERVER } from '@/constants'

jest.mock('@/util/networking')
makeRequest.mockImplementation(() => Promise.resolve())

describe('nomadMaps', () => {
  test('getTraces send request to the "traces" endpoint with correct parameters', async () => {
    expect.assertions(1)
    getTraces(0)
    await flushPromises()
    expect(makeRequest).toHaveBeenCalledWith(NOMAD_MAPS_SERVER, 'traces', { expeditions: 0 })
  })

  test('getSteps send request to the "steps" endpoint with correct parameters', async () => {
    expect.assertions(1)
    getSteps(0)
    await flushPromises()
    expect(makeRequest).toHaveBeenCalledWith(NOMAD_MAPS_SERVER, 'steps', { expeditions: 0 })
  })

  test('getExpeditions send request to the "expeditions" endpoint with undefined as parameter if no expedition id is provided', async () => {
    expect.assertions(1)
    getExpeditions()
    await flushPromises()
    expect(makeRequest).toHaveBeenCalledWith(NOMAD_MAPS_SERVER, 'expeditions', undefined)
  })

  test('getExpeditions send request to the "expeditions" endpoint with ids parameter if an array of ids is provided', async () => {
    expect.assertions(1)
    getExpeditions(['0', '1', '2'])
    await flushPromises()
    expect(makeRequest).toHaveBeenCalledWith(NOMAD_MAPS_SERVER, 'expeditions', { ids: '0,1,2' })
  })

  test('getUsers send request to the "users" endpoint with undefined as parameter if no user id is provided', async () => {
    expect.assertions(1)
    getExpeditions()
    await flushPromises()
    expect(makeRequest).toHaveBeenCalledWith(NOMAD_MAPS_SERVER, 'expeditions', undefined)
  })

  test('getUsers send request to the "users" endpoint with ids parameter if an array of ids is provided', async () => {
    expect.assertions(1)
    getExpeditions(['0', '1', '2'])
    await flushPromises()
    expect(makeRequest).toHaveBeenCalledWith(NOMAD_MAPS_SERVER, 'expeditions', { ids: '0,1,2' })
  })
})
