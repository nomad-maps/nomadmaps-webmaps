
import { getSteps } from '@/api/nomadMaps'

import {
  FETCH_STEPS_ERROR,
  FETCH_STEPS_REQUEST,
  FETCH_STEPS_SUCCESS
} from './mutations.type'

import { FETCH_STEPS } from './actions.type.js'

export default {
  [FETCH_STEPS]: async function (context, payload) {
    context.commit(FETCH_STEPS_REQUEST)

    try {
      const steps = await getSteps(payload)
      context.commit(FETCH_STEPS_SUCCESS, steps)
    } catch (e) {
      context.commit(FETCH_STEPS_ERROR, e)
    }
  }
}
