
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import cloneDeep from 'lodash.clonedeep'
import flushPromises from 'flush-promises'
import storeConfig from '../module'
import { getSteps } from '@/api/nomadMaps'
import { FETCH_STEPS } from '../actions.type'

import {
  FETCH_STEPS_ERROR,
  FETCH_STEPS_REQUEST,
  FETCH_STEPS_SUCCESS
} from '../mutations.type'

jest.mock('@/api/nomadMaps')

const localVue = createLocalVue()
localVue.use(Vuex)

function createSteps () {
  const arr = new Array(3)
  return arr.fill().map((step, i) => ({ properties: { id: `a${i}`, name: 'step', number: i } }))
}

describe('steps module', () => {
  const steps = createSteps()
  const featuresCollection = { features: cloneDeep(steps) }
  let store = null

  beforeEach(() => {
    const clonedStoreConfig = cloneDeep(storeConfig)
    store = new Vuex.Store(clonedStoreConfig)
  })

  test('dispatching FETCH_STEPS updates steps getter', async () => {
    expect.assertions(1)
    getSteps.mockImplementation(() => Promise.resolve(featuresCollection))
    store.dispatch(FETCH_STEPS)
    await flushPromises()
    expect(store.getters.steps).toEqual(steps)
  })

  test('committing FETCH_STEPS_REQUEST updates stepsFetchingOngoing getter', async () => {
    expect(store.getters.stepsFetchingOngoing).toBe(false)
    store.commit(FETCH_STEPS_REQUEST)
    expect(store.getters.stepsFetchingOngoing).toBe(true)
  })

  test('committing FETCH_STEPS_SUCCESS updates stepsFetchingOngoing getter', async () => {
    expect(store.getters.stepsFetchingOngoing).toBe(false)
    store.commit(FETCH_STEPS_REQUEST)
    expect(store.getters.stepsFetchingOngoing).toBe(true)
    store.commit(FETCH_STEPS_SUCCESS, { features: [] })
    expect(store.getters.stepsFetchingOngoing).toBe(false)
  })

  test('committing FETCH_STEPS_ERROR updates stepsFetchingError getter', async () => {
    expect(store.getters.stepsFetchingError).toBe(null)
    store.commit(FETCH_STEPS_ERROR, 'error')
    expect(store.getters.stepsFetchingError).not.toBe(null)
  })
})
