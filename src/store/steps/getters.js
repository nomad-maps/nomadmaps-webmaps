
export default {
  steps: state => state.entities.byId === null ? []
    : Object.keys(state.entities.byId)
      .map(id => state.entities.byId[id])
      .sort((step1, step2) => step1.properties.number < step2.properties.number ? -1 : 1),

  stepsFetchingError: state => state.entities.fetchingError,
  stepsFetchingOngoing: state => state.entities.isFetching
}
