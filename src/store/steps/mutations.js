
import {
  FETCH_STEPS_ERROR,
  FETCH_STEPS_REQUEST,
  FETCH_STEPS_SUCCESS
} from './mutations.type'

export default {
  [FETCH_STEPS_ERROR] (state, error) {
    state.entities.isFetching = false
    state.entities.fetchingError = error
  },

  [FETCH_STEPS_REQUEST] (state) {
    state.entities.isFetching = true
  },

  [FETCH_STEPS_SUCCESS] (state, geojson) {
    state.entities.isFetching = false

    state.entities.byId = geojson.features.reduce((all, feat) => {
      all[feat.properties.id] = feat
      return all
    }, {})
  }
}
