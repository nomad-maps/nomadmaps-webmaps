
import { transformExtent } from 'ol/proj'
import basemaps from '@/constants/basemaps'
import mutations from './mutations'
import getters from './getters'

const state = {
  mapBasemap: basemaps.POSITRON,
  mapfeatureHilighted: [],
  mapfeatureSelected: [],
  mapHomeExtent: transformExtent([-180, -90, 180, 90], 'EPSG:4326', 'EPSG:3857'),
  mapModal: false,
  mapVisibleLayers: { traces: true, photos: true, osm: true, mapillary: true },
  popupPosition: null,
  selectedTool: null,
  transitionRequestQueue: []
}

export default {
  state,
  mutations,
  getters
}
