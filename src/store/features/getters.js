
export default {
  mapBasemap: state => state.mapBasemap,
  mapfeatureHilighted: state => state.mapfeatureHilighted,
  mapfeatureSelected: state => state.mapfeatureSelected,
  mapHomeExtent: state => state.mapHomeExtent,
  mapModal: state => state.mapModal,
  mapVisibleLayers: state => state.mapVisibleLayers,
  popupPosition: state => state.popupPosition,
  selectedTool: state => state.selectedTool,
  transitionRequestQueue: state => state.transitionRequestQueue
}
