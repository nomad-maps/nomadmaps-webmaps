
import {
  BACK_HOME,
  HIDE_MAP_MODAL,
  HIDE_POPUP,
  HILIGHT_MAP_FEATURES,
  SELECT_MAP_FEATURE,
  SELECT_TOOL,
  SET_BASEMAP,
  SET_HOME_EXTENT,
  SET_POPUP_POSITION,
  SHOW_MAP_MODAL,
  TOGGLE_LAYER_VISIBILITY,
  TRANSITION_REQUEST_PUSH,
  TRANSITION_REQUEST_SHIFT
} from './mutations.type.js'

export default {
  [BACK_HOME] (state) {
    state.transitionRequestQueue = state.transitionRequestQueue.concat({
      type: 'fit',
      extent: state.mapHomeExtent,
      options: { duration: 1000, padding: [80, 10, 10, 60] }
    })
  },

  [HIDE_MAP_MODAL] (state) {
    state.mapModal = false
  },

  [HIDE_POPUP] (state) {
    state.popupPosition = null
    state.photosSelected = []
    state.osmDataSelected = []
    state.mapillarySequencesSelected = []
    state.mapillaryImagesSelected = []
    state.mapfeatureSelected = []
  },

  [HILIGHT_MAP_FEATURES] (state, payload) {
    state.mapfeatureHilighted = payload
  },

  [SELECT_MAP_FEATURE] (state, payload) {
    state.mapfeatureSelected = payload
  },

  [SELECT_TOOL] (state, value) {
    state.selectedTool = state.selectedTool === value ? null : value
  },

  [SET_BASEMAP] (state, payload) {
    state.mapBasemap = payload
  },

  [SET_HOME_EXTENT] (state, payload) {
    state.mapHomeExtent = payload
  },

  [SET_POPUP_POSITION] (state, payload) {
    state.popupPosition = payload
    state.osmDataSelected = []
    state.photosSelected = []
    state.mapillarySequencesSelected = []
    state.mapillaryImagesSelected = []
  },

  [SHOW_MAP_MODAL] (state) {
    state.mapModal = true
  },

  [TOGGLE_LAYER_VISIBILITY] (state, payload) {
    state.mapVisibleLayers[payload] = !state.mapVisibleLayers[payload]
  },

  [TRANSITION_REQUEST_PUSH] (state, payload) {
    state.transitionRequestQueue = state.transitionRequestQueue.concat(payload)
  },

  [TRANSITION_REQUEST_SHIFT] (state, payload) {
    state.transitionRequestQueue = state.transitionRequestQueue.slice(1)
  }
}
