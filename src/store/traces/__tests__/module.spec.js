
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import cloneDeep from 'lodash.clonedeep'
import flushPromises from 'flush-promises'
import storeConfig from '../module'
import { getTraces } from '@/api/nomadMaps'
import { FETCH_TRACES } from '../actions.type'

import {
  FETCH_TRACES_ERROR,
  FETCH_TRACES_REQUEST,
  FETCH_TRACES_SUCCESS
} from '../mutations.type'

jest.mock('@/api/nomadMaps')

const localVue = createLocalVue()
localVue.use(Vuex)

function createTraces () {
  const arr = new Array(3)
  return arr.fill().map((step, i) => ({ properties: { id: `a${i}`, name: 'trace', number: i } }))
}

describe('traces module', () => {
  const traces = createTraces()
  const featuresCollection = { features: cloneDeep(traces) }
  let store = null

  beforeEach(() => {
    const clonedStoreConfig = cloneDeep(storeConfig)
    store = new Vuex.Store(clonedStoreConfig)
  })

  test('dispatching FETCH_TRACES updates traces getter', async () => {
    expect.assertions(1)
    getTraces.mockImplementation(() => Promise.resolve(featuresCollection))
    store.dispatch(FETCH_TRACES)
    await flushPromises()
    expect(store.getters.traces).toEqual(traces)
  })

  test('committing FETCH_TRACES_REQUEST updates tracesFetchingOngoing getter', async () => {
    expect(store.getters.tracesFetchingOngoing).toBe(false)
    store.commit(FETCH_TRACES_REQUEST)
    expect(store.getters.tracesFetchingOngoing).toBe(true)
  })

  test('committing FETCH_TRACES_SUCCESS updates tracesFetchingOngoing getter', async () => {
    expect(store.getters.tracesFetchingOngoing).toBe(false)
    store.commit(FETCH_TRACES_REQUEST)
    expect(store.getters.tracesFetchingOngoing).toBe(true)
    store.commit(FETCH_TRACES_SUCCESS, { features: [] })
    expect(store.getters.tracesFetchingOngoing).toBe(false)
  })

  test('committing FETCH_TRACES_ERROR updates tracesFetchingError getter', async () => {
    expect(store.getters.tracesFetchingError).toBe(null)
    store.commit(FETCH_TRACES_ERROR, 'error')
    expect(store.getters.tracesFetchingError).not.toBe(null)
  })
})
