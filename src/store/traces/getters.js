
export default {
  traces: state => state.entities.byId === null ? []
    : Object.keys(state.entities.byId)
      .map(k => state.entities.byId[k]),

  tracesFetchingError: state => state.entities.fetchingError,
  tracesFetchingOngoing: state => state.entities.isFetching
}
