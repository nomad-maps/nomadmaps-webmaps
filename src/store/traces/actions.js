
import { getTraces } from '@/api/nomadMaps'

import {
  FETCH_TRACES
} from './actions.type'

import {
  FETCH_TRACES_ERROR,
  FETCH_TRACES_REQUEST,
  FETCH_TRACES_SUCCESS
} from './mutations.type'

export default {
  [FETCH_TRACES]: async function (context, payload) {
    context.commit(FETCH_TRACES_REQUEST)

    try {
      const traces = await getTraces(payload)
      context.commit(FETCH_TRACES_SUCCESS, traces)
    } catch (e) {
      context.commit(FETCH_TRACES_ERROR, e)
    }
  }
}
