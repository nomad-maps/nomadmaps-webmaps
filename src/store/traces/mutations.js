
import {
  FETCH_TRACES_ERROR,
  FETCH_TRACES_REQUEST,
  FETCH_TRACES_SUCCESS
} from './mutations.type'

export default {
  [FETCH_TRACES_ERROR] (state, payload) {
    state.entities.isFetching = false
    state.entities.fetchingError = payload
  },

  [FETCH_TRACES_REQUEST] (state) {
    state.entities.isFetching = true
  },

  [FETCH_TRACES_SUCCESS] (state, payload) {
    state.entities.isFetching = false
    state.entities.byId = payload.features.reduce((acc, val) => {
      acc[val.properties.id] = val
      return acc
    }, {})
  }
}
