
import {
  FETCH_USERS_ERROR,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS
} from './mutations.type'

export default {
  [FETCH_USERS_ERROR] (state, error) {
    state.isFetching = false
    state.fetchingError = error
  },

  [FETCH_USERS_REQUEST] (state) {
    state.isFetching = true
  },

  [FETCH_USERS_SUCCESS] (state, json) {
    state.isFetching = false

    state.byId = json.reduce((acc, user) => {
      acc[user.id] = user
      return acc
    }, {})
  }
}
