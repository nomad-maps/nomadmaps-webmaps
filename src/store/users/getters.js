
export default {
  users: state => state.byId === null ? []
    : Object.keys(state.byId)
      .map(id => state.byId[id]),

  usersById: state => state.byId,

  usersFetchingError: state => state.fetchingError,
  usersFetchingOngoing: state => state.isFetching
}
