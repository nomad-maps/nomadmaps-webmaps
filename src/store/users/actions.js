
import { getUsers } from '@/api/nomadMaps'

import {
  FETCH_USERS_ERROR,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS
} from './mutations.type'

import { FETCH_USERS } from './actions.type.js'

export default {
  [FETCH_USERS]: async function (context, payload) {
    const ids = payload || []

    // Get users already fetched
    const userIds = context.getters.users
      .map(u => u.id)

    // Keep ids of users that have not already been fetched
    const toFetch = ids
      .filter(id => userIds.indexOf(id) < 0)

    // Fetch users only if no payload has been provided (meaning we have
    // to fetch all users) or if some users have not been fetched yet
    if (!payload || toFetch.length > 0) {
      context.commit(FETCH_USERS_REQUEST)

      try {
        const users = await getUsers(toFetch)
        context.commit(FETCH_USERS_SUCCESS, users)
      } catch (e) {
        context.commit(FETCH_USERS_ERROR, e)
      }
    }
  }
}
