
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import cloneDeep from 'lodash.clonedeep'
import flushPromises from 'flush-promises'
import storeConfig from '../module'
import { getUsers } from '@/api/nomadMaps'
import { FETCH_USERS } from '../actions.type'

import {
  FETCH_USERS_ERROR,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS
} from '../mutations.type'

jest.mock('@/api/nomadMaps')

const localVue = createLocalVue()
localVue.use(Vuex)

function createUsers () {
  const arr = new Array(3)
  return arr.fill().map((expedition, i) => ({ id: `a${i}`, name: 'user', number: i }))
}

describe('users module', () => {
  const users = createUsers()
  let store = null

  beforeEach(() => {
    const clonedStoreConfig = cloneDeep(storeConfig)
    store = new Vuex.Store(clonedStoreConfig)
  })

  test('dispatching FETCH_USERS updates users getter', async () => {
    expect.assertions(1)
    getUsers.mockImplementation(() => Promise.resolve(users))
    store.dispatch(FETCH_USERS)
    await flushPromises()
    expect(store.getters.users).toEqual(users)
  })

  test('dispatching FETCH_USERS updates usersById getter', async () => {
    expect.assertions(1)
    getUsers.mockImplementation(() => Promise.resolve(users))
    store.dispatch(FETCH_USERS)
    await flushPromises()
    const ids = Object.keys(store.getters.usersById)
    expect(ids).toEqual(['a0', 'a1', 'a2'])
  })

  test('committing FETCH_USERS_REQUEST updates usersFetchingOngoing getter', async () => {
    expect(store.getters.usersFetchingOngoing).toBe(false)
    store.commit(FETCH_USERS_REQUEST)
    expect(store.getters.usersFetchingOngoing).toBe(true)
  })

  test('committing FETCH_USERS_SUCCESS updates usersFetchingOngoing getter', async () => {
    expect(store.getters.usersFetchingOngoing).toBe(false)
    store.commit(FETCH_USERS_REQUEST)
    expect(store.getters.usersFetchingOngoing).toBe(true)
    store.commit(FETCH_USERS_SUCCESS, [])
    expect(store.getters.usersFetchingOngoing).toBe(false)
  })

  test('committing FETCH_USERS_ERROR updates usersFetchingError getter', async () => {
    expect(store.getters.usersFetchingError).toBe(null)
    store.commit(FETCH_USERS_ERROR, 'error')
    expect(store.getters.usersFetchingError).not.toBe(null)
  })
})
