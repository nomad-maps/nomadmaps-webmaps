import Vue from 'vue'
import Vuex from 'vuex'
import expeditions from './expeditions/module'
import features from './features/module'
import steps from './steps/module'
import traces from './traces/module'
import users from './users/module'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    expeditions,
    features,
    steps,
    traces,
    users
  }
})
