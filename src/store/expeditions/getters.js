
export default {
  expeditions: state => state.byId === null ? []
    : Object.keys(state.byId)
      .map(id => state.byId[id]),

  expeditionsById: state => state.byId,

  expeditionsFetchingError: state => state.fetchingError,
  expeditionsFetchingOngoing: state => state.isFetching
}
