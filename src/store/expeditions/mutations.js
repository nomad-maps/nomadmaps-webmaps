
import {
  FETCH_EXPEDITIONS_ERROR,
  FETCH_EXPEDITIONS_REQUEST,
  FETCH_EXPEDITIONS_SUCCESS
} from './mutations.type'

export default {
  [FETCH_EXPEDITIONS_ERROR] (state, error) {
    state.isFetching = false
    state.fetchingError = error
  },

  [FETCH_EXPEDITIONS_REQUEST] (state) {
    state.isFetching = true
  },

  [FETCH_EXPEDITIONS_SUCCESS] (state, json) {
    state.isFetching = false

    state.byId = json.reduce((acc, expedition) => {
      acc[expedition.id] = expedition
      return acc
    }, {})
  }
}
