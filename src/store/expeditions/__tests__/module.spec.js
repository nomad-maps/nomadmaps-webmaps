
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import cloneDeep from 'lodash.clonedeep'
import flushPromises from 'flush-promises'
import storeConfig from '../module'
import { getExpeditions } from '@/api/nomadMaps'
import { FETCH_EXPEDITIONS } from '../actions.type'

import {
  FETCH_EXPEDITIONS_ERROR,
  FETCH_EXPEDITIONS_REQUEST,
  FETCH_EXPEDITIONS_SUCCESS
} from '../mutations.type'

jest.mock('@/api/nomadMaps')

const localVue = createLocalVue()
localVue.use(Vuex)

function createExpeditions () {
  const arr = new Array(3)
  return arr.fill().map((expedition, i) => ({ id: `a${i}`, name: 'expedition', number: i }))
}

describe('expeditions module', () => {
  const expeditions = createExpeditions()
  let store = null

  beforeEach(() => {
    const clonedStoreConfig = cloneDeep(storeConfig)
    store = new Vuex.Store(clonedStoreConfig)
  })

  test('dispatching FETCH_EXPEDITIONS updates expeditions getter', async () => {
    expect.assertions(1)
    getExpeditions.mockImplementation(() => Promise.resolve(expeditions))
    store.dispatch(FETCH_EXPEDITIONS)
    await flushPromises()
    expect(store.getters.expeditions).toEqual(expeditions)
  })

  test('dispatching FETCH_EXPEDITIONS updates expeditionsById getter', async () => {
    expect.assertions(1)
    getExpeditions.mockImplementation(() => Promise.resolve(expeditions))
    store.dispatch(FETCH_EXPEDITIONS)
    await flushPromises()
    const ids = Object.keys(store.getters.expeditionsById)
    expect(ids).toEqual(['a0', 'a1', 'a2'])
  })

  test('committing FETCH_EXPEDITIONS_REQUEST updates expeditionsFetchingOngoing getter', async () => {
    expect(store.getters.expeditionsFetchingOngoing).toBe(false)
    store.commit(FETCH_EXPEDITIONS_REQUEST)
    expect(store.getters.expeditionsFetchingOngoing).toBe(true)
  })

  test('committing FETCH_EXPEDITIONS_SUCCESS updates expeditionsFetchingOngoing getter', async () => {
    expect(store.getters.expeditionsFetchingOngoing).toBe(false)
    store.commit(FETCH_EXPEDITIONS_REQUEST)
    expect(store.getters.expeditionsFetchingOngoing).toBe(true)
    store.commit(FETCH_EXPEDITIONS_SUCCESS, [])
    expect(store.getters.expeditionsFetchingOngoing).toBe(false)
  })

  test('committing FETCH_EXPEDITIONS_ERROR updates expeditionsFetchingError getter', async () => {
    expect(store.getters.expeditionsFetchingError).toBe(null)
    store.commit(FETCH_EXPEDITIONS_ERROR, 'error')
    expect(store.getters.expeditionsFetchingError).not.toBe(null)
  })
})
