
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export const state = {
  fetchingError: null,
  isFetching: false,
  byId: null
}

export default {
  state,
  actions,
  mutations,
  getters
}
