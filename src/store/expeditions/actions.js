
import { getExpeditions } from '@/api/nomadMaps'

import {
  FETCH_EXPEDITIONS_ERROR,
  FETCH_EXPEDITIONS_REQUEST,
  FETCH_EXPEDITIONS_SUCCESS
} from './mutations.type'

import { FETCH_EXPEDITIONS } from './actions.type.js'

export default {
  [FETCH_EXPEDITIONS]: async function (context, payload) {
    const ids = payload || []

    // Get expeditions already fetched
    const expIds = context.getters.expeditions
      .map(exp => exp.id)

    // Keep ids of expeditions that have not already been fetched
    const toFetch = ids
      .filter(id => expIds.indexOf(id) < 0)

    // Fetch expeditions only if no payload has been provided (meaning we have
    // to fetch all expeditions) or if some expeditions have not been fetched yet
    if (!payload || toFetch.length > 0) {
      context.commit(FETCH_EXPEDITIONS_REQUEST)

      try {
        const expeditions = await getExpeditions(toFetch)
        context.commit(FETCH_EXPEDITIONS_SUCCESS, expeditions)
      } catch (e) {
        context.commit(FETCH_EXPEDITIONS_ERROR, e)
      }
    }
  }
}
