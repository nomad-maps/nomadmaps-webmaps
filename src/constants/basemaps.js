
export default {
  OSM: {
    attributions: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    crossOrigin: 'anonymous',
    type: 'xyz',
    url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
  },
  POSITRON: {
    attributions: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
    crossOrigin: 'anonymous',
    type: 'xyz',
    url: 'http://{a-d}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png'
  },
  BING: {
    type: 'bing'
  }
}
