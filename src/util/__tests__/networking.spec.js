
import flushPromises from 'flush-promises'
import { makeRequest } from '../networking'
import axios from 'axios'

jest.mock('axios')

describe('networking', () => {
  test('makeRequest send HTTP request to the correct url and with the correct parameters', async () => {
    expect.assertions(1)
    axios.mockImplementation(() => Promise.resolve({ status: 200, data: [] }))
    makeRequest('http://example.com', 'test', { params: '0' })
    await flushPromises()

    expect(axios).toHaveBeenCalledWith({
      method: 'get',
      url: 'http://example.com/test',
      data: { params: '0' }
    })
  })

  test('makeRequest returns only payload', async () => {
    const res = { status: 200, data: [{ id: 0 }] }
    expect.assertions(1)
    axios.mockImplementation(() => Promise.resolve(res))
    const data = await makeRequest('http://example.com', 'test')
    await flushPromises()
    expect(data).toEqual(res.data)
  })

  test('HTTP response with a status code different from 200 throw a "Server issue" exception', async () => {
    const res = { status: 404, data: [] }
    expect.assertions(1)
    axios.mockImplementation(() => Promise.resolve(res))

    await expect(makeRequest('http://example.com', 'test'))
      .rejects
      .toThrow()
  })
})
