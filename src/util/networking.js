/**
 * The util/networking module gathered functions dealing with network
 */

import axios from 'axios'

function createRequestOptions (server, endPoint, params) {
  const options = {
    method: 'get',
    url: `${server}/${endPoint}`
  }

  if (params) {
    options.data = params
  }

  return options
}

async function sendRequest (options) {
  let resp

  try {
    resp = await axios(options)
  } catch (e) {
    throw new Error('Network issue')
  }

  if (resp.status !== 200) {
    throw new Error('Server issue')
  }

  return resp.data
}

export async function makeRequest (server, endPoint, params) {
  const options = createRequestOptions(server, endPoint, params)

  try {
    return sendRequest(options)
  } catch (e) {
    throw e
  }
}
