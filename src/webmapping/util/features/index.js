
import GeoJSON from 'ol/format/GeoJSON'

export const findFeatureById = function (layer, featureId) {
  const found = layer
    .getSource()
    .getFeatures()
    .filter(f => f.get('id') === featureId)

  return found.length > 0 ? found[0] : null
}

export const sortByYCoordinate = function (feature1, feature2) {
  const y1 = feature1.getGeometry().getCoordinates()[1]
  const y2 = feature2.getGeometry().getCoordinates()[1]
  return y1 < y2 ? 1 : -1
}

export const toGeoJSON = function (olFeature) {
  return JSON.parse((new GeoJSON()).writeFeature(olFeature))
}

export default {
  findFeatureById,
  sortByYCoordinate,
  toGeoJSON
}
