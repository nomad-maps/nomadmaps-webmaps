
import { transformExtent } from 'ol/proj'
import GeoJSON from 'ol/format/GeoJSON'
import { MAPILLARY_CLIENT_ID } from '@/constants'

const parser = new GeoJSON({
  dataProjection: 'EPSG:4326',
  featureProjection: 'EPSG:3857'
})

export function dataReader (data) {
  const features = parser.readFeatures(data)
  features.forEach(f => { f.set('id', f.get('key')) })
  return features
}

export function mapillaryUrlBuilder (clientId, username, type) {
  return function (extent) {
    const reprojExtent = transformExtent(extent, 'EPSG:3857', 'EPSG:4326')
    const urlExtent = reprojExtent.join(',')
    const server = 'https://a.mapillary.com/v3/'

    return `${server}${type}?bbox=${urlExtent}&usernames=${username}&client_id=${clientId}&per_page=1000`
  }
}

export function sequencesUrlBuilder (username) {
  return mapillaryUrlBuilder(MAPILLARY_CLIENT_ID, username, 'sequences')
}

export function imagesUrlBuilder (username) {
  return mapillaryUrlBuilder(MAPILLARY_CLIENT_ID, username, 'images')
}

export default {
  dataReader,
  imagesUrlBuilder,
  sequencesUrlBuilder
}
