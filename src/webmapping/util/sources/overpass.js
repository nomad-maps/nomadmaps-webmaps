
import { transformExtent } from 'ol/proj'
import OSMXML from 'ol/format/OSMXML'

const OSMParser = new OSMXML()

export function dataReader (data) {
  const features = OSMParser.readFeatures(data, {
    dataProjection: 'EPSG:4326',
    featureProjection: 'EPSG:3857'
  })

  features.forEach(f => f.set('id', f.id_))
  return features
}

export function urlBuilder (uid) {
  return function (extent) {
    const reprojExtent = transformExtent(extent, 'EPSG:3857', 'EPSG:4326')
    const urlExtent = [reprojExtent[1], reprojExtent[0], reprojExtent[3], reprojExtent[2]].join(',')
    const server = 'https://overpass-api.de/api/interpreter'

    return `${server}?data=(node(${urlExtent})(uid:${uid});way(${urlExtent})(uid:${uid});>;);out+meta;`
  }
}

export default {
  dataReader,
  urlBuilder
}
