
import flickr from './flickr'
import mapillary from './mapillary'
import overpass from './overpass'

export default {
  flickr,
  mapillary,
  overpass
}
