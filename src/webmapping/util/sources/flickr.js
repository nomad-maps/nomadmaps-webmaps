
import { transformExtent } from 'ol/proj'
import GeoJSON from 'ol/format/GeoJSON'
import { FLICKR_API_KEY } from '@/constants'

const parser = new GeoJSON({
  dataProjection: 'EPSG:4326',
  featureProjection: 'EPSG:3857'
})

export function dataReader (data) {
  const features = data.photos.photo.map(ph => ({
    type: 'Feature',
    geometry: { type: 'Point', coordinates: [parseFloat(ph.longitude), parseFloat(ph.latitude)] },
    properties: { id: ph.id, title: ph.title, url_s: ph.url_s, url_l: ph.url_l, date: ph.datetaken }
  }))

  return parser.readFeatures({ type: 'FeatureCollection', features })
}

export function urlBuilder (userId) {
  return function (extent) {
    const reprojExtent = transformExtent(extent, 'EPSG:3857', 'EPSG:4326')
    const bbox = reprojExtent.map(n => n > 0 ? '+' + n : '' + n).join(',')
    const server = 'https://api.flickr.com/services/rest/'
    const method = 'flickr.photos.search'
    const apiKey = FLICKR_API_KEY
    const extras = 'geo,url_s,url_l,date_taken'

    return `${server}?method=${method}&api_key=${apiKey}&user_id=${userId}&bbox=${bbox}&extras=${extras}&format=json&nojsoncallback=1`
  }
}

export default {
  dataReader,
  urlBuilder
}
