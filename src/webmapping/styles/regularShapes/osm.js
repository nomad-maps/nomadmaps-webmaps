
import RegularShape from 'ol/style/RegularShape'
import Fill from 'ol/style/Fill'

export default function (color, iconPath) {
  const regShape = new RegularShape({
    fill: new Fill({ color: [255, 255, 255, 1] }),
    radius: 12,
    snapToPixel: false
  })

  const canvas = regShape.getImage()
  const ctx = canvas.getContext('2d')
  const image = new Image(14, 14)

  image.onload = () => { ctx.drawImage(image, 5, 5) }
  image.src = iconPath

  ctx.fillStyle = color
  ctx.strokeStyle = 'rgb(255, 255, 255)'
  ctx.lineWidth = 4
  ctx.beginPath()
  ctx.arc(12, 12, 10, 0, 2 * Math.PI)
  ctx.closePath()
  ctx.stroke()
  ctx.fill()

  return regShape
}
