
import Style from 'ol/style/Style'
import photo from './regularShapes/photo'

const regShape = photo('rgba(236, 74, 148, 1)')

export default function (feature, res) {
  return new Style({ image: regShape })
}
