
import mapillary from './mapillary'
import mapillaryHilighted from './mapillaryHilighted'
import osm from './osm'
import osmHilighted from './osmHilighted'
import photo from './photo'
import photoHilighted from './photoHilighted'
import step from './step'
import stepHilighted from './stepHilighted'
import trace from './trace'
import traceHilighted from './traceHilighted'

export default {
  mapillary,
  mapillaryHilighted,
  osm,
  osmHilighted,
  photo,
  photoHilighted,
  step,
  stepHilighted,
  trace,
  traceHilighted
}
