
import Style from 'ol/style/Style'
import pin from './icons/pin'

export default new Style({
  image: pin('rgb(249, 237, 50)'),
  zIndex: 0
})
