import Style from 'ol/style/Style'
import Fill from 'ol/style/Fill'
import Stroke from 'ol/style/Stroke'
import Circle from 'ol/style/Circle'

export default new Style({
  fill: new Fill({ color: [236, 74, 148, 0.8] }),
  image: new Circle({
    fill: new Fill({ color: [236, 74, 148, 1] }),
    radius: 8,
    stroke: new Stroke({
      color: [255, 255, 255, 0.8],
      width: 2
    })
  }),
  stroke: new Stroke({
    color: [236, 74, 148, 0.8],
    width: 4
  })
})
