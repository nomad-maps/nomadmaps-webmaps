
import Icon from 'ol/style/Icon'

export default function (label) {
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')

  ctx.font = '14px sans-serif'
  ctx.textAlign = 'center'

  const textWidth = ctx.measureText(label).width
  const rectX = 56 - (textWidth / 2)

  ctx.fillStyle = 'rgb(65, 64, 66)'
  ctx.fillRect(rectX, 0, textWidth + 8, 24)

  ctx.beginPath()
  ctx.moveTo(52, 24)
  ctx.lineTo(68, 24)
  ctx.lineTo(60, 32)
  ctx.closePath()
  ctx.fill()

  ctx.fillStyle = '#fff'
  ctx.fillText(label, 60, 16)

  return new Icon({
    anchor: [0.5, 3.3],
    img: canvas,
    imgSize: [120, 32]
  })
}
