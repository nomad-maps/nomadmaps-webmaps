
import Icon from 'ol/style/Icon'

export default function (color) {
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')

  const shadowGradient = ctx.createRadialGradient(10, 57, 10, 10, 57, 0)
  shadowGradient.addColorStop(0, 'rgba(255, 255, 255, 0)')
  shadowGradient.addColorStop(0.5, 'rgba(128, 128, 128, 0.5)')
  shadowGradient.addColorStop(1, 'rgba(0, 0, 0, 1)')

  const pinHeadGradient = ctx.createRadialGradient(8, 7, 5, 7, 7, 0)
  pinHeadGradient.addColorStop(0, color)
  pinHeadGradient.addColorStop(1, 'rgb(255, 255, 255)')

  ctx.lineWidth = 2
  ctx.strokeStyle = '#737373'

  ctx.fillStyle = shadowGradient
  ctx.fillRect(0, 45, 20, 20)

  ctx.fillStyle = color

  ctx.beginPath()
  ctx.moveTo(10, 20)
  ctx.lineTo(10, 57)
  ctx.stroke()

  ctx.beginPath()
  ctx.moveTo(0, 0)
  ctx.arc(10, 10, 10, 0, 2 * Math.PI)
  ctx.fill()

  ctx.fillStyle = pinHeadGradient
  ctx.fillRect(3, 2, 10, 10)

  return new Icon({
    anchor: [0.5, 1],
    img: canvas,
    imgSize: [20, 65]
  })
}
