
import Icon from 'ol/style/Icon'

export default function (color, zIndex) {
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')

  const shadowGradient = ctx.createRadialGradient(10, 40, 6, 10, 40, 0)
  shadowGradient.addColorStop(0, 'rgba(255, 255, 255, 0)')
  shadowGradient.addColorStop(0.5, 'rgba(128, 128, 128, 0.5)')
  shadowGradient.addColorStop(1, 'rgba(128, 128, 128, 1)')

  const pinHeadGradient = ctx.createRadialGradient(8, 7, 5, 7, 7, 0)
  pinHeadGradient.addColorStop(0, color)
  pinHeadGradient.addColorStop(1, 'rgba(255, 255, 255, 0.5)')

  ctx.lineWidth = 2
  ctx.strokeStyle = '#737373'

  ctx.fillStyle = shadowGradient
  ctx.fillRect(2, 32, 16, 16)

  ctx.beginPath()
  ctx.moveTo(10, 20)
  ctx.lineTo(10, 40)
  ctx.stroke()

  ctx.fillStyle = color
  ctx.strokeStyle = 'rgb(255, 255, 255)'
  ctx.lineWidth = 1

  ctx.beginPath()
  ctx.moveTo(0, 0)

  ctx.beginPath()
  ctx.arc(10, 10, 10, 0, 2 * Math.PI)
  ctx.closePath()
  ctx.fill()

  ctx.fillStyle = pinHeadGradient
  ctx.fillRect(3, 2, 10, 10)

  canvas.style.zIndex = '' + zIndex

  return new Icon({
    anchor: [0.5, 1],
    crossOrigin: 'anonymous',
    img: canvas,
    imgSize: [20, 48]
  })
}
