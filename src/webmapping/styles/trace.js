
import Style from 'ol/style/Style'
import Stroke from 'ol/style/Stroke'

export default new Style({
  stroke: new Stroke({
    color: [249, 237, 50, 1],
    lineDash: [5, 10],
    width: 5
  }),
  zIndex: 0
})
