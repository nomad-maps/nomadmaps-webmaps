import Style from 'ol/style/Style'
import Fill from 'ol/style/Fill'
import Stroke from 'ol/style/Stroke'
import osm from './regularShapes/osm'

const regShape = osm('rgb(236, 74, 148)', '/icons/osm_white.svg')

export default function (feature, res) {
  const y = feature.getGeometry().getCoordinates()[1]
  const type = feature.getGeometry().getType()
  const zIndex = type === 'Point' ? -(Math.round(y / 10)) : -Infinity

  return new Style({
    fill: new Fill({ color: [236, 74, 148, 0.5] }),
    image: regShape,
    stroke: new Stroke({ color: [236, 74, 148, 1], width: 2 }),
    zIndex
  })
}
