
import Style from 'ol/style/Style'
import pin from './icons/pin'
import pinLabel from './icons/pinLabel'

export default function (feature, res) {
  return [
    new Style({
      image: pin('rgb(236, 74, 148)'),
      zIndex: 0
    }),
    new Style({
      image: pinLabel(feature.get('name')),
      zIndex: 0
    })
  ]
}
